<p hidden id="comments-section-id">misc-owncloud-vs-nextcloud</p>
# One area where Owncloud is better than Nextcloud. 

There is one area that is not talked about as much in regards to how Nextcloud and Ownclowd have deverged after they forked. That area is their desktop sync client, especially when running on windows. 

The Owncloud client has delta sync capability, while the Nextcloud client does not. Delta sync means that it can upload only the parts of the file that have changed, rather than transfering the entire file. On the other hand, the Nextcloud client cannot even download multiple files in parallel, see [issue #976](https://github.com/nextcloud/desktop/issues/976).

The Owncloud desktop app manages to fit in smaller than a reasonable 20mb for the Windows and Mac editions, and a tiny 1.5mb for the Debian package. On the other hand, the Nextcloud Windows installer is weighs in at a chonky 100mb+, and the Mac edition is about 80mb. The Nextcloud desktop Linux image is also about 100mb, but it not directly comparable since it is an appimage, which is much more compatible across Linux distros, but means that it has to bundle more things inside the image. 

Now, onto the Windows versions specifically. 

The Owncloud client comes by default as a MSI installer file, which is IMO, about the same as an EXE installer for the average user, but miles better for a sysadmin or someone using something like Chocolatey. They are even nice enough to provide options for whether to add a desktop shortcut, to not add the shell extension, to disable auto update, to launch after installation, the installation directory, and whether to reboot if needed. 

On the other hand, the Nextcloud client comes as a NSIS EXE installer. It can be silently installed, and you can change the installation directory, but nothing else can be specified with install arguments, unlike the plethora available with Owncloud. There is an open about this [issue #59](https://github.com/nextcloud/desktop/issues/59). To disable auto update, you either have to add a registry value (eew), or create/modify the configuration file for each and every user on the computer (double eew). 

To be fair, neither of them allow specifying the server URL as an installation parameter, which would be really nice. Also, the Nextcloud client has has some extra features added, such as being able to launch the browser directly into the talk app, and other Nextcloud apps. 

Nextcloud does have a client MSI installer, but it only [provides it to people paying for their client branding plus service](https://help.nextcloud.com/t/branded-nextcloud-clients/64023). Their branding service is an option addon if you are paying for one of their higher tiers of support, and it ["starts at €6000 yearly"](https://nextcloud.com/pricing/#featureblock-branding), which seems to indicate that their branding plus which includes MSI builds is even more expensive. On the up side, it looks like they are working on [making the MSI build scripts and resources available publicly](https://github.com/nextcloud/desktop/pull/2369).

## Update 2020-11-06

Well, it seems like they are being nice and they have publicly released MSI binaries for version 3.03.
https://github.com/nextcloud/desktop/releases/tag/v3.0.3

And it has plenty of public properties, so it is definitely configurable. 

The Owncloud client still has delta sync and is smaller, but the gap is a lot smaller now.