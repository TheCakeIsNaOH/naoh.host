<p hidden id="comments-section-id">misc-leechblock-school</p>
# Help keep kids focused on school with Leechblock
So given the current circumstances, some people are now tasked with managing keeping kids focused on remote schoolwork. Some of you reading this are one of those people, or you may be managing the computers for one of those people. This is a guide for how to provide a bit of technological assistance to help keep the students focused.

#### Disclaimer / Philosophy

The idea is to limit the sites available during the school day on the computer that the child does schoolwork. This way, the if person that is overseeing them sees that they are on the schoolwork computer, they can be reasonably confident that they are at least on the correct page, and not on social media or similar on a different tab or window. That is it. This is not intended to work as an all encompassing parental control solution. The block should either be set to only be active during the school day, or the child should have access to another device that does not have this block. If the computer that you want to set up the block on is owned by the child, this should be something that is done with their permission because they want the help on focusing, and IMO, it should be done after discussion with the child, especially if the child is actually a teenager. This also completely does not work if the child has access to administrator/root.

#### Software

This will work on Windows and Linux machines, and possibly it could be adapted to MacOS. It would be compatible with ChromeOS if you managed to get enterprise polices on it, which I think requires paying Google for a business account and then enrolling the device with that account. 

The central software to this guide is called [leechblock](https://www.proginosko.com/leechblock/). It is available as a [chromium extension](https://chrome.google.com/webstore/detail/leechblock-ng/blaaajhemilngeeffpbfkdjjoefldkok) or as a [firefox extension](https://addons.mozilla.org/en-US/firefox/addon/leechblock-ng/). It is designed to provide a roadblock to visiting selected sites, or visiting them too much, or during certain times. It is explicitly not designed for parental control, but this is not quite standard parental control, so with a bit of supporting help in browser and os configuration, it works.

The other main thing you need is enterprise polices for your browser of choice. 
Google has a decent reference for Chrome polices [here](https://cloud.google.com/docs/chrome-enterprise/policies), and it also mostly applies to other chromium browsers, just with a change of where you stick the polices. Microsoft has a reference [here](https://docs.microsoft.com/en-us/deployedge/microsoft-edge-policies), it is very similar to Chrome polices, but there are more changes that with other chromium builds from what I saw. You can go to `chrome://policy` or `edge://policy/` to see a list of active polices.
For Firefox, there is a list of polices [here](https://github.com/mozilla/policy-templates), with general documentation [here](https://support.mozilla.org/en-US/products/firefox-enterprise/policies-customization-enterprise/policies-overview-enterprise). There is also a neat extension that has a GUI to generate the `policies.json` file [here](https://addons.mozilla.org/en-US/firefox/addon/enterprise-policy-generator/). You can go to `about:policies` to see active, inactive and errored polices.

#### OS configuration. 

First up, the computer needs to have only one account that the child has access to. Other accounts, especially admin accounts need to have passwords, else the changes can be reverted fairly easily.

Next, unless you want to configure multiple browsers, the child's account should have only one browser accessible. On Linux, the simplest way to do is just uninstall all other browsers, or you could prevent the child's account from accessing the binaries by using UNIX permissions or by something more advanced like AppArmor. On Windows, this is kind of a pain to deal with because the new Microsoft Edge can't be uninstalled if you are on a new enough OS version. In this case, the best thing to do is probably setup the polices for it as well, and set Leechblock to block everything.

### Policy configuration

#### Chromium
##### Windows

Polices are managed by group policy. This can be edited in the registry, or via the GUI if you install the [admx templates](https://www.chromium.org/administrators/policy-templates)

For Chrome they are under `HKLM\Software\Policies\Google\Chrome\`
For Chromium they are under `HKLM\Software\Policies\Chromium\` 
For Edge they are under`HKLM\Software\Policies\Microsoft\Edge`

You can also put them in `HKCU`if you want a per-user policies as apposed to the `HKLM` for system wide polices.

Here is an example reg file:
```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\Software\Policies\Google\Chrome]
;Disable incognito
"IncognitoModeAvailability"=dword:00000001
;Disable deleting history
"AllowDeletingBrowserHistory"=dword:00000001
;Disable guest mode
"Browser​Guest​Mode​Enabled"=dword:00000000
;Force sign in
"​Browser​Signin"=dword:00000002

;Force install leechblock
[HKEY_LOCAL_MACHINE\Software\Policies\Google\Chrome\ExtensionInstallForcelist]
"1"="blaaajhemilngeeffpbfkdjjoefldkok"
```
The values should work for pretty much any chromium based browser, just change the folder depending on what browser it is.
The main exception is Edge, which has `"InPrivateModeAvailability"`instead of `"IncognitoModeAvailability"`

##### Linux
Stick these json files in a specific folder in `/etc`, which folder it is depends on the chromium build/type
For default chromium use `/etc/chromium/policies/managed/`
For chrome use`/etc/opt/chrome/policies/managed/`

Force install leechblock:
```
{
    "ExtensionInstallForcelist": [ "blaaajhemilngeeffpbfkdjjoefldkok" ]
}

```

Force signing in to a google account:

```
{
        "​Browser​Signin" : 2
}
```

Do not allow browser history deletion:

```
{
        "AllowDeletingBrowserHistory" : False
}

```

Do not allow guest mode
```
{
        "Browser​Guest​Mode​Enabled" : False
}
```

Do not allow incognito mode 

```
{
        "IncognitoModeAvailability" : 1
}
```

#### Firefox

Json based, generally stick everything in one file called `polices.json`. On Windows, create a folder called `distribution`and put the json file in there. On Linux, it is generally put in `/etc/firefox/policies`for system wide polices, or a folder called distribution next to the installation works as well. Look up your specific distribution. 

Here is an example `polices.json`, but I would suggest generating your own [with the extension]((https://addons.mozilla.org/en-US/firefox/addon/enterprise-policy-generator/)

```
{
  "policies": {
    "DisableFirefoxAccounts": true,
    "DisableFirefoxStudies": true,
    "DisablePrivateBrowsing": true,
    "DisableProfileRefresh": true,
	"DisableForgetButton": true,
    "DisableSafeMode": true,
    "ExtensionSettings": {
      "*": {
        "allowed_types": [
          "extension",
          "theme",
          "dictionary",
          "langpack"
        ],
        "installation_mode": "allowed"
      },
      "leechblockng@proginosko.com": {
        "install_url": "https://addons.mozilla.org/firefox/downloads/latest/leechblock-ng/latest.xpi",
        "installation_mode": "force_installed"
      }
    },
    "OverrideFirstRunPage": "",
    "OverridePostUpdatePage": "",
    "Preferences": {
      "datareporting.policy.dataSubmissionPolicyBypassNotification": true,
      "media.gmp-gmpopenh264.enabled": true,
      "media.gmp-widevinecdm.enabled": true
    },
    "PromptForDownloadLocation": true
  }
}
```

#### Leechblock configuration

In general settings, change it to require a password and add a password. 

If you want a whitelist, then in a block set, first enter a `*`on the first line of the domain names to block. This switches it to a whitelist instead of a blacklist. Then enter the domains you want unblocked, each prefixed with a +, and each on it's own line.

Otherwise, just put the domains you want to be not accessible in the domain names to block section.

Then setup when to block, which days and what times.  Finally, select immediately block pages once blocking conditions are met, then hit save. 

There are plenty of other settings to play around with, but that should be enough to start. Keep and eye and make sure that the required sites are all unblocked, because I guarantee that not all of the sites required will be whitelisted initially. 