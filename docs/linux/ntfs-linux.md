<p hidden id="comments-section-id">ntfs-linux</p>
# NTFS on Linux

<sub><sub>Originally posted 2019-01-18</sub></sub>

So I recently bought an external 8tb WD elements drive to for backups from my Linux NAS. I decided to try using NTFS as the FS on it since I wanted the backups to be as compatible as possible with both Linux and Windows.

My backup script is below

         newdate=`date +%g%m%e`
         mkdir /mnt/8tb/$newdate
         cp -al /mnt/8tb/olddate/* /mnt/8tb/$newdate/
         rsync -avh --delete    /sharedfolders/ /mnt/8tb/$newdate

Rsync runs at about 30-40mb per second, with about 70-80 of one core being used. That sucks since the drive is capable of 150~mb per second on windows. 

Also, after backing up my over 5tb of data, I moved it to a windows machine, and discovered that the drive was 88% fragmented! That is crazy.
![windows-defrag-drives.png](/../_media/windows-defrag-drives.png) 


---


My mount options-

- `defaults`
- `noauto` - only mount when I want it to
- `user` -  allow user mount
- `uid=1000`, `gid=100`, and `umask=000` -  Set permissions wide open as desired for this drive
- `hide_dot_files` and `hide_hid_files` - sync the different ways to make the file hidden
- `windows_names` - make sure windows does not puke at unsupported characters
- `noatime` and `big_writes` - somewhat successful tuning parameters

---

So overall, I will probably go with another FS next time and just live with not being able to access it on windows, or only accessible with a third party program install.

--- 

