<p hidden id="comments-section-id">linux-minecraft-velocity</p>
# How to reverse proxy your Minecraft server with Velocity

  
### Intro  
 
So given Wendell's recent video+[thread](https://forum.level1techs.com/t/haproxy-wi-run-lots-of-public-services-on-your-home-server/159335) on HaProxy-Wi and other content relating to self hosting services at home and then proxy-ing them to the internet, I have seen some questions about running a reverse proxy with a Minecraft server, and answered them, and thought I might as well make a full thread.

The issue is that unlike many other services, Minecraft is not HTTP based, so HaProxy and similar software does not work. Therefore, either you have to directly forward all requests, or use a specific proxy software for Minecraft, which is where [Velocity](https://velocitypowered.com/) comes in. Velocity also has other features, such as allowing server linking, and supporting plugins. 

The setup is fairly easy. Install java 8+, download the jar, run it once to create an example config file, edit the config file, run the jar again and you are good to go. Their wiki [explains this part fairly well.](https://velocitypowered.com/wiki/users/getting-started/)

Once you get it up and running, if you are using it on a Linux machine, I would suggest also setting up the systemd service, as shown below. If you are running on a non-systemd distro, you probably already know how to write a service, or at least know enough to look up the info easily.

### Systemd service

Autostarting after boot, and easy restarting are two of the benefits of using a systemd service.

Example systemd service file:
```
[Unit]
Description=Velocity Minecraft Proxy

[Service]
WorkingDirectory=/path/to/velocity/folder
ExecStart=/path/to/java -Xms512M -Xmx512M -XX:+UseG1GC -XX:G1HeapRegionSize=4M -XX:+UnlockExperimentalVMOptions -XX:+ParallelRefProcEnabled -XX:+AlwaysPreTouch -jar velocity-proxy.jar 
User=velocity
Type=simple
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```

Edit the`WorkingDirectory`, `ExecStart`, and `User` as needed. Call the file `velocity.service`, put in `/etc/systemd/system/`, although there other places that will work. 

Run
```bash
#Reload service files from disk
sudo systemctl daemon-reload
#Set the velocity service to start at boot
sudo systemctl enable velocity
#Start the service
sudo systemctl start velocity
```

and you should be in business with a systemd managed proxy. It will then autostart with the system, and is easy to restart(`sudo systemctl restart velocity`)

### Update script

Here is a update script I wrote. I am not super confident in the long term reliability of this, because it scrapes the download page to get the link, so if they update their build system or website, then this might need to be changed. But it works for the moment. 

 It works if you are using the stable version. Also you need to make a `version.txt` in your velocity directory before this will work. Something like `echo 1.1.0 > version.txt`, or whatever the current version is. Edit the `/path/to/velocity/directory` as needed, as well as the name of the jar file. 

```bash
#!/bin/bash

#Bash unofficial strict mode
set -euo pipefail

#Easier to CD than to use absolute paths 
cd /path/to/velocity/directory

#Scrape the version and url from the downloads page
UPDATE_PAGE=$(curl -s https://velocitypowered.com/downloads/)
UPDATE_URL=$(echo $UPDATE_PAGE | grep -Eoi 'https://versions.velocitypowered.com/download/[0-9.]{0,6}jar')
UPDATE_VERSION=$(echo $UPDATE_URL | rev | cut -d/ -f1 | rev | xargs basename -s .jar)

#Get the current version from the version.txt file
CURRENT_VERSION=$(cat version.txt)

#Check if an update is needed
if [ $CURRENT_VERSION == $UPDATE_VERSION ]
then
        echo Velocity up to date: version $CURRENT_VERSION
else
        echo New Velocity update available: version $UPDATE_VERSION
        systemctl stop velocity.service
        #Moves the current jar to a jar.old so you could manually revert if the download fails
        mv velocity-proxy.jar velocity-proxy.jar.old
        curl -o velocity-proxy.jar $UPDATE_URL
        systemctl start velocity.service
        echo $UPDATE_VERSION > version.txt
        touch velocity-proxy.jar.old
        rm velocity-proxy.jar.old
fi
```

Note: Script updated 2021-03-17 to account for changes in the velocitypowered.com website