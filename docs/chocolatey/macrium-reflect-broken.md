<p hidden id="comments-section-id">chocolatey-reflect-free</p>
# Macrium Reflect Free has the most creatively broken installer I have ever seen
<sub><sub>Originally posted 2020-01-08</sub></sub>

So, I use Macrium Reflect occasionally for cloning systems to a new drive and the Chocolatey package was unmaintained. So I decided to try to make a package for the current version, which has turned into an adventure.

---
When you download the free version from their website, you are not downloading the software, you are downloading a downloader, so I checked if I could include the actual software in the package and not download it separately. Unfortunately, that was a no according to their license.

I then pulled out the proxy software to see if I could grab a URL to download directly from, which again is a no, they have something special setup that downloads in multiple chunks from AWS and recombines them.

Next up was to try to see if there was any way to quietly run the installer, which was unsuccessful. So on to AutoHotkey, it was, although fortunately, I found an unmerged PR on the previous maintainer's repository that had a script that still worked, although needed a bit of tweaking.

Finally, I had the actual installer downloading automatically, next to run it.

---

I found the official documentation [on how to run the installer silently](https://knowledgebase.macrium.com/display/KNOW72/Installing+Macrium+Reflect+from+the+command+line), but this is where the fun really starts.

I tried it on my system, the silent install arguments worked fine, great I added it to the package. But no they can't make it that easy. When I tested it in the chocolatey testing environment, it appeared to work, but then I discovered that I was not installing. I spent a bunch of time trying to figure out why the silent arguments were not working, before trying to run it non-silent and having it error out that the free version was not compatible windows server. WTF, it does not error when run silently on window server, it completes fine, just not actually installing anything. So that being broken, I added a manual check that errors the package when running on windows server.

Right, so I start testing in windows 10 VMs, the install seems to be unreliable, but I am done with installing so I move on to the uninstall section for a break. Then, of course, I discover that the uninstall seems to be designed to break the automatic Chocolatey install because it gives two uninstall strings in the registry which then makes Chocolatey think that the search for the string is broken so it aborts. Ok, time to make a manual uninstall script, which went ok-ish.

Back to the install, I kept playing with it and discovered that the silent install only works if Macrium Reflect was previously installed on the system. WTF again, it fails with no indication that something went wrong. Ok, I found that there is a log option, the log says something about the key not being found. WTF again, this is the free version, it does not ask for a key when run with the GUI. Poking around in the registry, it does store a key there when installed, so ok it still has a product key even in the free version but it somehow does not generate/acquire when run silently, and then fails without an error. 

Ok, let's see how hard the installer is to make an AHK script for, and it is very hard, I gave up at least for a while.

Next up, try to contact their support to see if they have any advice. And I discovered that all of their support contracts and also their forum are only available to those with a paid version.

---

After some more work I figured out the silent install. 

The exe is a wrapper for a MSI file. It extracts the MSI by default to `C:\windows\installer\reflect_setup<version>.msi`

One of the custom actions in the MSI is to get the product key from the CLI arguments if the installer is run without the GUI. This is not needed for the free edition, in fact is is what breaks the free editions because they do not come with a key.

The way to fix it is to create a patch that drops the `NonInteractiveLicenseInit` from the `InstallExecuteSequence`, there are some programs that can patch the MSI directly, but the better way to do it to share is to create an MST transform.

The MST transform can be created in a number of programs. Orca is included in the windows SDK. Master Packager is another free option.
