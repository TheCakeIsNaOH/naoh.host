<p hidden id="comments-section-id">chocolatey-quiet-drivers</p>
## How to quietly install non-WHQL driver(s)

Some programs install drivers, and some of those drivers do not have a WHQL signature by Microsoft. This is an issue because if a driver does not have a WHQL signature, and the certificate is not in the trusted store, and it will pop up a message asking if you want to trust the certificate. This is a problem since Chocolatey packages are supposed to be quiet  (i.e. no user interaction required and no notifications displayed on the screen).

You can fix this by making sure that the certificate is imported into the trusted store before you install the program that has the driver.

### Getting the certificate
There are two good ways you can get the driver:

1. Install the program (while always trusting the publisher), then open up `certlm.msc`, go to trusted publishers>certificates then export the installed cert, and include inside the Chocolatey package.
2. If the certificate is signing one of the files, you can extract it during the install script using something like this: 

```
$driverFile = Join-Path $toolsDir 'driver.cat'
$outputFile = Join-Path $toolsDir 'cert.cer'
$exportType =[System.Security.Cryptography.X509Certificates.X509ContentType]::Cert
$cert = (Get-AuthenticodeSignature $driverFile).SignerCertificate;
[System.IO.File]::WriteAllBytes($outputFile, $cert.Export($exportType)
```
([source](https://web.archive.org/web/20190411195856/https://stackoverflow.com/questions/36775331/extract-certificate-from-sys-file))

If you use the first method, it is best practice to include a note as to where you got the extracted certificate. Also, you have to make sure to update it when the developer of the program switches certificates.

The second method is more reliable over time after updates, as you don't have and included certificate to update, however it requires that the certificate be available in a signature of the installer of the program, or the driver itself (if it is available separately).

### Installing the certificate
Once you have the certificate, you can use `certutil` to install the certificate. 
```
certutil -addstore -f "TrustedPublisher" $toolsdir\<certNameHere>.cer 
```

## Installing a driver from an .inf

If you have a bare driver that has an .inf to install manually, use `pnputil` like this:
```
pnputil -i -a $toolsDir\driver.inf
```

This is the older syntax, but it is what is available Windows versions before 10. And on the community repository, all packages should be compatible with windows 7 if possible.