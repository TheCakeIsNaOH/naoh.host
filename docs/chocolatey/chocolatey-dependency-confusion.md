<p hidden id="comments-section-id">choco-dependency-confusion</p>
# Chocolatey and Dependency Confusion

## What is Dependency Confusion?

Read this: [Dependency Confusion: How I Hacked Into Apple, Microsoft and Dozens of Other Companies](https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610)

TLDR, if you have a package manager, with both a private/internal repository and a public repository configured, 
someone could upload a malicious package to the public repository with the same name as a package on your private repository, 
but a higher version. Then, you could install the malicious package from the public repository instead of the one from your private repository.

## Is it applicable to Chocolatey?

Yes. 

The public Chocolatey.org community repository is enabled by default with Chocolatey, 
and many organizations (and some individuals) using Chocolatey have private repositories.
Also, the Chocolatey officially recommended repositories (Nexus, Proget, Artifactory, MyGet) 
all have the capability to act as a caching proxy for the Chocolatey.org repository.

## It is as bad with Chocolatey as it is with other types of packages managers?

Not as far as I can tell, for a number of reasons:

- Chocolatey packages uploaded to Chocolatey.org are run through a virustotal scan.
- Release versioned Chocolatey packages uploaded to Chocolatey.org are moderated by human moderators 
before being made generally available. (Prerelease versioned packages are automatically made available)
- It is a lot less likely for internal package names to leak as compared to many other affected package managers, 
as Chocolatey packages are generally not libraries that are built into a public product, 
like nuget or npm packages often are
- Licensed Chocolatey editions have antivirus integration built in, so all software downloaded is scanned.

## How to mitigate this?

The first step is, of course to disable the chocolatey.org source on your Chocolatey installs. 

The second is to disable or filter the chocolatey.org proxy connection on your private repository. 
If you are only using custom private packages, then you are done here. 
But if you are like me, the community repository has too many useful packages to ignore. 

There are two major options that I see for how to safely use community repository packages without having having to worry about dependency confusion. 

The first is to completely disable proxying to chocolatey.org, and manually download selected packages and then upload them to your repository. 
If you have a C4B license, then this seems to fit right in with the automation around internalizing packages, and should be doable with minor tweaks to your automations.
If you do not have a C4B license, you could automate this with a couple of scripts, like my [choco-remixer](https://github.com/TheCakeIsNaOH/choco-remixer) project does.

The second option is to create all of your private packages with a prefix (or suffix), and then blacklist that prefix from being downloaded from the proxy connection.
This should be doable in Sonatype Nexus with [Routing rules](https://blog.sonatype.com/new-feature-repository-routing), in Inedo ProGet using [package promotion](https://docs.inedo.com/docs/proget/packages/package-promotion#promotion-feed), 
and hopefully there is some equivalent if you are using another repository software

### What about prefix reservation

Nuget.org has something called [prefix reservation](https://docs.microsoft.com/en-us/nuget/nuget-org/id-prefix-reservation), where an organization can reserve a prefix for their exclusive use. 
This seems like a reasonable mitigation strategy, however this [is not implemented by chocolatey.org](https://gitter.im/chocolatey/chocolatey.org?at=602d285ddc108172e781cf72).

## What could be done on the Chocolatey.org end?

Currently, the virustotal scan on each Chocolatey.org package is checked manually by moderators, 
but an automatic check is under development. 
I am not sure if this is planned to remove pre-release packages if they fail, but it should. No link at the moment, because chocolatey.org recently went closed-source.

Another thing is that the Chocolatey people have been dropping hints like mad for the past couple 
years that there will be a repository specifically for C4B license holders (and hints that it is planned for this year).
Which will have packages that are held to a higher standard than the community repository. When this comes out, assuming that you have a C4B license, you can just switch over to the business repository, and you are done.

I also have a couple other things I thought of that could help mitigate this, but to help prevent the possibility anyone abusing them, I ain't sharing publicly (yet).

## Conclusion

Yep, this is an issue, but if you were already careful with what you allowed in your private repository, it's not really a big deal.