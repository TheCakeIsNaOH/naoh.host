<p hidden id="comments-section-id">win7-dwm</p>
# Patching Windows 7 Desktop Window Manager

<sub><sub>Originally posted 2018-11-05</sub></sub>

You know this message?

![windows-7-dwm-error](/../_media/win-7-dwm-error.jpg ':size=451x265') 

The one that shows up when you are playing a game and it is running great until it pops up and throws you out of the game. The one where the "don't show again" button does NOT work correctly. Yes, that message.

I have been playing Just Cause 3 recently, and this \*\*\*\*\*\* shows up soon after I start the game every reboot. Not only do I have to acknowledge the message, when I alt-tab back into the game, it is in windowed mode but the settings menu say it is in full-screen. This means I have to go into the graphics menu, set it to windowed, only then can I set it back to fullscreen! JC3 also has super high memory usage(7+gb), brain-dead default controller bindings that cannot be edited in-game an example of which is having right yaw and jump out of a plane mapped to the same button, and a game-breaking bug that effects not one but two story missions and  they did not patch it, even when they later released DLC.

/rant, let's get back on topic. 

----

It will show up when using an application that is demanding on vRAM, which is generally a game. Even though the machine is running fine, it will show up.

There are a couple of solutions to this. 
1. Keep clicking keep the current scheme -  not really a solution
2. Globally disable desktop composition - not a good option
3. Disable desktop composition per affected application - can work for some people, but not every program likes this, and it is not a good solution for people who alt-tab out of games frequently
4. Disable Windows Troubleshooting messages - not a good option as it also disables other important messages
5. Patch dwm.exe - the most difficult and dangerous but the best technically and the one I will be showing you how to do.

---

**Thanks to [nars](https://superuser.com/users/712844/nars) at superuser for figuring this out and posting it [here](https://superuser.com/questions/314570/disable-do-you-want-to-change-the-color-scheme-to-improve-performance-warning)**

I am just re-packaging his solution with more detail on the how-to part.

**This is not a basic or Microsoft approved procedure, do it at your own risk**


**This for only a fully updated windows 7 x64 install**


I might later update to include x86 installs.

Read instructions through before starting

---

1. Download and install [HxD](https://mh-nexus.de/en/hxd/) or your Hex editor of choice.

2. Download and install your checksum utility of choice. I have used [MD5 & SHA Checksum Utility](https://raylin.wordpress.com/downloads/md5-sha-1-checksum-utility/) and [Hashtab](http://implbits.com/products/hashtab/) in the past

3. If possible, open this page on another device or print off this page.

4. Go to c:\windows\system32 and copy dwm.exe to a convenient folder, such as your desktop. Rename it to dwm-orig.exe or similar.

5. Using your checksum utility make sure dwm.exe matches these-
5a.  MD5 `c206c9dbfc34afd367dd150d979a5185`
5b. SHA1 `8590d6c80e8d8e5371f48f04c53939b7b3debd09`
5c. If it does not match these, stop so you do not damage your install

6. Make another copy, call it dwm-patched.exe or a similar name

7. Open dwm-patched.exe in your Hex editor. Search in in Hex-values for `FF1562B1000085C079`

8. Replace the above string with `9090909090909090EB`and save.

9. Check that dwm-patch.exe has a checksum of the following-
9a. MD5 `8243c03dad2b9aaaddabe65e4869e2ae`
9b. SHA1 `c8e0fcb3829d95191d96ad485e7b10d74f6d394f`

10. Open services.msc and stop Desktop Window Manager Session Manager

11. Go back to c:\windows\system32 and open file properties for dwm.exe. Go to the security tab, then click advanced, then the owner tab, then click edit. Change the owner to the administrators group by clicking administrators, applying, then ok-ing all of the windows.

12. Re-open the security tab of properties of dwm.exe, click edit then allow the administrators full control. Ok the edit window, then click advanced then change permissions then click allow inheritable permissions, then ok all the windows. 

13. Make a copy of dwm-patched.exe and name it dwm.exe

14. Exit ALL programs you have open except the explorer windows in system32. You may also have to open task manager, end explorer.exe then click file>run then run explorer.exe, and finally reopen your system32 explorer window.

15. Copy dwm.exe on your desktop(or other folder you used) to c:\windows\system32

16. Delete the dwm.exe on your desktop(or other folder you used)

17. Open services.msc and start Desktop Window Manager Session Manager

18. You are done. You can re-open your applications, although a restart would not be amiss. Keep both your patched and orig executables.

19. You can revert the patch by making a copy of dwm-orig.exe called dwm.exe and copying it to system32

---

It is possible that windows will revert the file in the future, well actually probable if you ever run sfc /scannow. If it happens, check the checksum(as is step #5), then redo steps #10-18.

If the checksum is different, then it is a different version of dwm.exe. In this case do not use the old version patched dwm.exe if you want to have a working system.
